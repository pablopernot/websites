---
date: 2016-11-16T00:00:00Z
type: meetup
slug: a-quoi-ca-sert-un-po
background: "20161116/meetup1.jpg"
title: 'À quoi ça sert un PO ?'
synopsis: "C'est quoi un PO finalement ? Qu'est ce que ce terme englobe ?"
type: "meetup"
---


## Les articles qui annonçaient ce premier meetup

* [Aller à l'école des PO](https://www.infoq.com/fr/news/2016/11/aller-a-l-ecole-des-po) par InfoQ
* [L'école des product owners](http://lucyinthescrum.com/lecole-des-product-owners/) par Lucie Lesage

## Les articles et documents sur ce premier meetup

* [À quoi ça sert un PO ?](http://areyouagile.com/2016/11/a-quoi-ca-sert-un-product-owner/) par Pablo Pernot
* [À quoi ça sert un PO ?](http://fr.slideshare.net/ddreptate/a-quoi-a-sert-un-product-owner) par Dragos Dreptate

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Les photos des présentations

<div class="row 50 uniform">

<div class="4u"><span class="image fit"><img src="/images/20161116/meetup4.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161116/meetup14.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161116/meetup11.jpg" alt=""></span></div>

<div class="4u"><span class="image fit"><img src="/images/20161116/meetup6.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161116/meetup7.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161116/meetup10.jpg" alt=""></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20161116/meetup12.jpg" alt=""></span></div>

<div class="4u"><span class="image fit"><img src="/images/20161116/meetup13.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161116/meetup16.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161116/meetup15.jpg" alt=""></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20161116/meetup3.jpg" alt=""></span></div>
</div>


## Les photos des "openspace"

Oui il nous manque les titres sur les résultats des brainstorming :) (amélioration continue). À vous de faire les liens... Ceux qui savent nous renseigner, j'ajouterai des titres des sessions...

<div class="box alt">
<div class="row uniform">
<div class="12u$"><span class="image fit"><img src="/images/20161116/meetup9.jpg" alt=""></span></div>

<div class="4u"><span class="image fit"><img src="/images/20161116/meetup21.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161116/meetup22.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161116/meetup23.jpg" alt=""></span></div>


<div class="4u"><span class="image fit"><img src="/images/20161116/meetup24.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161116/meetup27.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161116/meetup26.jpg" alt=""></span></div>

<div class="4u"><span class="image fit"><img src="/images/20161116/meetup28.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161116/meetup20.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161116/meetup30.jpg" alt=""></span></div>


<div class="4u"><span class="image fit"><img src="/images/20161116/meetup29.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161116/meetup32.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161116/meetup31.jpg" alt=""></span></div>


<div class="4u"><span class="image fit"><img src="/images/20161116/meetup33.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161116/meetup2.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161116/meetup34.jpg" alt=""></span></div>


</div>
</div>


