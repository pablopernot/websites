<?php

$to      = 'pablo.pernot@protonmail.com,dragos.dreptate@benextcompany.com,nils.lesieur@benextcompany.com';
$subject = '[SchoolOfPo] Message';
$headers = 'From: pablo.pernot@benextcompany.com' . "\r\n" .
'Reply-To: pablo.pernot@benextcompany.com' . "\r\n" .
'X-Mailer: PHP/' . phpversion();

$human = intval(htmlspecialchars($_POST['human'])); 

// Le message
$message = "Nom : " . htmlspecialchars($_POST['name']) . "\r\n"; 
$message .= "Mail : " . htmlspecialchars($_POST['email']) . "\r\n"; 
$message .= "Message : " . htmlspecialchars($_POST['message']) . "\r\n"; 
$message .= "Page : " . htmlspecialchars($_POST['page']) . "\r\n"; 

// Dans le cas où nos lignes comportent plus de 70 caractères, nous les coupons en utilisant wordwrap()
$message = wordwrap($message, 70, "\r\n");

// Envoi du mail
if ($human == 8) {
	mail($to, $subject, $message, $headers); 
	header('Location: /contact/ok.html');
} else {
	header('Location: /contact/ko.html');
}

?>
