<?php

$to      = 'pablo.pernot@benextcompany.com';
$subject = 'Value Driven Scaling';
$headers = 'From: pablo.pernot@benextcompany.com' . "\r\n" .
'Reply-To: pablo.pernot@benextcompany.com' . "\r\n" .
'X-Mailer: PHP/' . phpversion();

// Le message
$message = "Nom : " . htmlspecialchars($_POST['name']) . "\r\n"; 
$message .= "Mail : " . htmlspecialchars($_POST['email']) . "\r\n"; 
$message .= "Message : " . htmlspecialchars($_POST['message']) . "\r\n"; 

// Dans le cas où nos lignes comportent plus de 70 caractères, nous les coupons en utilisant wordwrap()
$message = wordwrap($message, 70, "\r\n");

// Envoi du mail
mail($to, $subject, $message, $headers); 

header('Location: index.html');

?>
