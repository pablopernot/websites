---
date: 2018-04-04
type: meetup
slug: entrainement-questionnements
background: "20180404/question1.jpg"
title: 'Entrainement aux questionnements'
synopsis: "Apprendre à définir le bon cadre avec les questions. Désapprendre à poser des questions sur les façons de faire. Apprendre à questionner pour faire avancer le PO dans l’organisation, dans l’entreprise."
---



## Liens

* [Entrainement aux questionnements](https://www.areyouagile.com/2018/04/entrainement-questionnements/)

## Par

<a href=https://twitter.com/nikepot target=_blank>Nicolas Potier</a>
<a href=https://twitter.com/pablopernot target=_blank>Pablo Pernot</a>

## Point clef

On passe des pizzas au traiteur grecque. Franc succès. 

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Quelques photos

<div class="row 50 uniform">
		
<div class="4u"><span class="image fit"><img src="/images/20180404/question7.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20180404/question6.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20180404/question5.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20180404/question4.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20180404/question3.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20180404/question2.jpg" alt="school of po"></span></div>

</div>



