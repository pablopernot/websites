---
date: 2017-06-27
type: meetup
slug: transmutateur
background: "20170627/transmutateur6.jpg"
title: 'Le transmutateur'
synopsis: "Le transmutateur cet étrange appareil qui va permettre aux codeurs de se faire comprendre par le PO et vice-verça. Mieux que la machine à remonter dans le temps, ou que celle qui transforme le plomb en or, le transmutateur permet de faire communiquer les codeurs et les PO."
---

Avec [Hedi Kallel](https://twitter.com/hkallel) et [Pablo Pernot](https://areyouagile.com)

## Les liens 

* Le dossier de travail de Transmutateur Inc sur nos formules pour PO : (... mince on a perdu le document initial...)
## La formule du finale 

<blockquote>
(P+V)/E + α
</blockquote>
Du pourquoi, de la valeur, un focus user, et une frugalité, simplification de la demande. 

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Quelques photos

<div class="row 50 uniform">

<div class="12u$"><span class="image fit"><img src="/images/20170627/transmutateur6.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur1.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur2.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170627/transmutateur3.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur4.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur5.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170627/transmutateur6.jpg" alt="school of po"></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20170627/transmutateur9.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur10.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur11.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170627/transmutateur12.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur13.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur14.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170627/transmutateur15.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur16.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170627/transmutateur17.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170627/transmutateur18.jpg" alt="school of po"></span></div>

</div>



