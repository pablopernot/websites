---
date: 2017-02-15
type: meetup
slug: user-story-mapping
background: "20170215/meetup2.jpg"
title: 'User Story Mapping'
synopsis: 'Le "User Story Mapping" est un outil étonnant car très flexible et adapté à de nombreux contextes, très facile à prendre en main et très utile. Présentation suivie par un forum ouvert qui sera aussi basé dessus (venez mapper vos idées).'
---

## Les liens 

* [Cartographie de plan d’action : le User Story Mapping](https://www.areyouagile.com/2017/01/cartographie-plan-action/)
* [Cartographie de plan d’action(user story map) revisitée](https://www.areyouagile.com/2017/02/cartographie-plan-action-revisitee/)
* [Où manger à Paris ?](http://alexthib.blogspot.fr/2006/02/ou-manger-paris.html)

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Les photos des présentations

<div class="row 50 uniform">

<div class="12u$"><span class="image fit"><img src="/images/20170215/meetup1.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170215/meetup2.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170215/meetup12.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170215/meetup3.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170215/meetup5.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170215/meetup6.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170215/meetup7.jpg" alt="school of po"></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20170215/meetup8.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170215/meetup9.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170215/meetup10.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170215/meetup11.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170215/meetup15.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170215/meetup13.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170215/meetup14.jpg" alt="school of po"></span></div>

</div>

## Les User Story Mapping - cartographie de plan d'action

<div class="row 50 uniform">

<div class="4u"><span class="image fit"><a href=/images/20170215/usm1.jpg>
<img src="/images/20170215/usm1.jpg" alt="school of po"></a></span></div>

<div class="4u"><span class="image fit"><a href=/images/20170215/usm2.jpg>
<img src="/images/20170215/usm2.jpg" alt="school of po"></a></span></div>

<div class="4u$"><span class="image fit"><a href=/images/20170215/usm3.jpg>
<img src="/images/20170215/usm3.jpg" alt="school of po"></a></span></div>

<div class="4u"><span class="image fit"><a href=/images/20170215/usm4.jpg>
<img src="/images/20170215/usm4.jpg" alt="school of po"></a></span></div>

<div class="4u"><span class="image fit"><a href=/images/20170215/usm5.jpg>
<img src="/images/20170215/usm5.jpg" alt="school of po"></a></span></div>
</div>
