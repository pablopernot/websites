---
date: 2017-07-19
type: meetup
slug: vision-produit
background: "20170719/vision9.jpg"
title: 'Eclairer la vision produit'
synopsis: "Un meetup proposé et animé par Ségolène Porot et Pablo Pernot qui s'adresse à vous les PO, les curieux, et autres intéressés par les enjeux stratégiques du produit, qui avez besoin d'un éclairage pour faire adhérer les parties prenante à votre produit."
---


## Les liens 

Un résumé là dans l'article de Ségolène : 

* [Retour sur la school of PO pour définir la vision produit](https://yourinspiraction.wordpress.com/2017/07/30/retour-sur-la-school-of-po-sur-la-vision-produit/)
* [La vidéo de Simon Sinek](https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action?language=fr)


## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Quelques photos

<div class="row 50 uniform">

<div class="4u"><span class="image fit"><img src="/images/20170719/vision8.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170719/vision7.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170719/vision6.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170719/vision5.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170719/vision4.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170719/vision3.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170719/vision10.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170719/vision11.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170719/vision12.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170719/vision1.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170719/vision2.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170719/vision9.jpg" alt="school of po"></span></div>

</div>



