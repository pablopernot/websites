---
title: Conférences
layout: content
headline: "School of PO, LA CONF"
---

## Conférence 2019 : "Product révolution"

![Product Révolution](/images/conference/revolution.png)

Ca y est la vente des billets pour la conférence 2019 est ouverte : [2019.schoolofpo.com](http://2019.schoolofpo.com). 
Feedback bienvenus.

Notez la date : 13 février 2019 

## Conférence 2018 : "Build me baby"

![Build me baby 2018](/images/2018.conf.jpg)

Le site de la conférence : [laconf.schoolofpo.com](https://laconf.schoolofpo.com/)



### Des articles

* [Inspiration, expertise & méthodo, comment transcender le rôle de product owner](http://connect.adfab.fr/event/inspiration-expertise-et-methodo-comment-transcender-le-role-de-product-owner)
* [Retour sur la conf School of PO](https://blog.eleven-labs.com/fr/retour-sur-la-conf-school-of-po/)
* [School of PO, retour sur la conférence](https://marmelab.com/blog/2018/02/15/la-conf-school-of-po.html)
* [Learnings from School Of PO Conference](https://medium.com/@souad.hadjiat/learnings-from-school-of-po-conference-1230b861208f)
