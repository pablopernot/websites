Témoignages
###########
:slug: temoignages
:date: 2014-02-02
:template: simple
:awesomefonticon: tree
:subtitle: Merci pour vos témoignages



Témoignages
===========


"Ce que m’a apporté la formation :

- une formation qui permet de se ressourcer, de prendre du recul sur les problématiques, avec les regards et échanges d’autres participants d’autres domaines
- revoir et approfondir des pratiques de définition de produit et de développement collaboratif
- progresser dans la façon d’enseigner et d’accompagner des équipes en formation ou dans la vraie vie des projets
- se projeter sur de nouvelles pistes de connaissances à acquérir, pour développer de nouvelles options à proposer selon le contexte
 
Ainsi, le raid pour moi c’est :

3 représentants majeurs de l’agilité en France donnent une image actualisée du développement agile tel qu’il se vit dans les organisations et une perspective éclairante du chemin à suivre pour progresser.

La complémentarité de leur vision en direct, est d’une richesse incomparable, même pour moi qui a pourtant l’habitude de lire leur production d’articles de blog, de livres et autres conférences."

-- **Jean-Pascal Boignard**


"Le raid agile est une belle expérience à plusieurs titres : un cadre magnifique, la rencontre avec des personnes partageant l’envie d’en savoir plus sur l’agilité et la bienveillance, et surtout des coachs motivés et motivant !

Je conseille ce raid à toute personne voulant s’imprégner de la culture agile, à ceux voulant apprendre à mener certains ateliers clefs dans la conception d’un produit.

En bonus, le soir, chacun peut partager avec les autres raiders une expérience, une connaissance autour d’un apéro bien mérité.

Ah oui, j’oubliais l’essentiel, les raiders sont délicieusement nourris par Christophe et sa femme. En résumé, de la bienveillance, de la nature, de l’agilité et de la bonne bouffe ! Cocktail gagnant !"

-- **Capucine Laverrière-Duclos**


"En plus de l’intérêt purement pédagogique et ludique du Raid, à travers ses multiples ateliers mais aussi et surtout à travers les discussions que l’on a pu avoir durant les balades ou les repas, j’y ai trouvé un autre intérêt, moins direct mais non moins important  : prendre du recul tout en restant immergé dans la culture agile, se remettre en question tout en se détendant, trouver de nouvelles idées. Pour moi, le Raid ça a été particulièrement ça. Ça et voir Pablo se faire éliminer au premier tour d’une partie de Loup Garou, bien sûr.

Dès mon retour au boulot, j’ai pu proposer de nouvelles idées sur des sujets que je ne voyais pas auparavant. Et ça m’a également permis de mettre en pratique quelques ateliers qui me paraissaient un peu obscur auparavant, comme l’impact mapping.

Pour ne rien gâcher, ambiance au top et accueil chaleureux de nos hôtes. Que demander de plus ?"

-- **Hédi Kallel**


"Étant le plus jeune du groupe et participant à ma première formation, j'ai eu le privilège de tomber sur le RAID Agile, de loin la meilleure formation qui puisse exister ! 3 jours de formation dans un cadre extra-ordinaire dans les hauteurs des Cévennes, là ou même le réseau n'a osé s'aventurer, Pablo et Claude nous ont pleinement immergé dans l'agilité avec des visions parfois opposées quand bien même convergentes. "

-- **Sofiène Laadhari**


"L'agilité n'est pas une méthodologie, elle dépend de son contexte, alors rien de tel qu'une immersion complète de 3 jours dans l'agilité pour en saisir toutes les nuances issues des deux différentes palettes de Pablo et Clodio. Mais surtout pour échanger entre nous et se rendre compte que nos problèmes ne sont pas si différents et pour découvrir comment chacun à ou va adapter l'agilité à son contexte pour les résoudre."

-- **Cyrille François**

"La **meilleure** formation que j'ai eue. Une immersion de 3 jours dans un cadre magnifique, des moments de partage et de réflexion qui amènent à une remise en question du 'management' et de la 'gestion de projet' tels que je les avais connus jusque là. Ce n'est pas seulement une formation professionnelle mais c'est aussi une remise en question personnelle qui recentre l'humain au centre des priorités. Merci également à Alex Boutin et Stéphane Langlois pour les discussions partagées."

-- **Alice Barralon** 

"J’ai adoré le Raid, c’est sans conteste une des meilleures formations que j’ai eues en quatorze ans de boutique, et une des rares dont je sois reparti avec quelques idées à tenter à court terme, en plus d’un bon coup de culture générale et d’une vision plus claire de ce qu’on peut espérer (ou pas) de l’agilité. Le changement de cadre et la vie en commun pendant ces trois jours me semblent être la bonne formule pour cette formation vu qu’il est – précisément – question de (re)mettre l’humain et la communication directe au cœur du système.

 
Le contenu était dense mais plutôt bien équilibré et privilégiait la mise en application directe et l’alternance avec des temps de discussion libre, et je pense qu’on a vu des choses dont la richesse se révèlera avec le temps. Avoir deux intervenants qui ne sont pas d’accord en tout est un vrai plus, évite une approche dogmatique et a bien contribué à démystifier et désacraliser l’univers agile. Contrairement à ce que je redoutais en arrivant, avoir deux tiers de participants venant de la même entreprise n’a pas du tout déséquilibré les débats et on a eu des échanges très riches avec tout le monde.

 
Cerise sur le gâteau, bonne ambiance, bonne chère (prévoyez un régime avant et après), beau temps, banjo au petit déj’ et jeux de société le soir.

 
Si la question *agile* vous intéresse, n’hésitez pas !"


-- **Matthieu Dazy**


"I just came back from the best Agile workshop ever! A 3-days bootcamp, dynamic, fun, packed with information. My head is full of plans to put it to good use, my legs are sore from hiking, and I probably gained 2 to 4 pounds in the process. A great place to meet new people, get to know your colleagues better, and improve your agile skills. It was AWESOME!" (see the post on linkedin_).

-- **Claire Cassan**

.. _linkedin: https://www.linkedin.com/pulse/raid-agile-claire-cassan

"Merci pour ce raid qui a dépassé toutes nos espérances à tout point de vue : ambiance, formation, randos ... et nourriture !"
 
-- **Ghis**

"Pour rester ou devenir Agile, de corps, d'esprit et pourquoi pas de cœur, rien ne vaut le Raid Agile proposé par Clodio et Pablo."

-- **Serge Prétet**

"Je m'étais inscrit au Raid Agile pour approfondir ma compréhension et mon approche des pratiques agiles. Objectif pleinement rempli: Claude et Pablo ont réussi à aborder toutes mes questions et celles des autres raideurs. Leur méthode se base sur des ateliers ludiques et participatifs et des conversations en mouvement dont les sujets sont choisis par les participants. De cette façon chacun s'enrichit de l'expérience des experts autant que de celles des autres membres du groupe. Auto organisation, agilité , bonne humeur, sport (rando, pétanque babyfoot) , tous ces ingrédients étaient dans la recette de la potion agile que les deux druides nous ont concoctée. Je conseille vivement ce breuvage à tous les agilistes débutants ou confirmés de Gaule ou d'ailleurs !"

-- **Nicolas Jouve**, Nodeslight_

.. _Nodeslight: http://www.nodeslight.fr/

"Je me suis inscrit au Raid Agile pour approfondir mes connaissances afin de préparer une bascule professionnelle vers une activité de Coach agile / Scrum Master.

Le Raid a pleinement répondu à mes attentes avec un programme riche, abordant l'ensemble des bonnes pratiques et les sujets du moment dans la sphère agile. L'animation du Raid est menée brillamment par 2 coachs agiles exceptionnels qui laissent une large place à l'échange d'idées avec les participants, ce qui permet d'enrichir les enseignements de retours d'expériences terrains favorisant ainsi l'identification des bonnes pratiques, des freins et des différentes solutions mises en place par les participants du Raid.

Plus qu'une simple découverte des différents outils et modes d'animations agiles à notre disposition, le Raid permet vraiment de progresser sur l'état d'esprit et sur les valeurs fondamentales de l'agilité via des discussions libres dans un cadre reposant et propice à la réflexion.

Et n'ayez crainte, le terme "Raid" est surtout utilisé pour marquer la différence de cette formation de 3 jours par rapport aux autres du marché. Au programme : des ateliers dans un gîte très agréable au cadre verdoyant, des balades dans la nature permettant d'échanger tout en s'oxygénant dans les paysages magnifiques des Cévennes, des jeux de société, de la pétanque, et des repas bons et copieux ;)

Bref, je recommande cette expérience à tous ceux qui veulent apprendre et progresser dans leur pratique de l'agilité. Quel que soit votre niveau, vous en ressortirez riche de nombreuses idées à tester dans votre contexte et plein d'énergie pour faire progresser votre organisation."
 
-- **Nicolas Montens**

"Le raidagile m’a permis de prendre du recul et de partager mes expériences avec nos coaches et d’autres personnes de différents secteurs, tailles, organisations. Après 2 ans de pratique, il m’a permis de progresser dans l’agilité, avec de nouveaux outils méthodologiques. 

Gaffe au cholestérol après le raid, mon toubib n’est pas content ! J "

-- **Jean-Claude Béard**

"Accessible à tous - même pour l'ascension du Mont Brion -, le Raid Agile m'a fait découvrir la culture Agile, son vocabulaire et ses bonnes pratiques à travers des jeux ludiques et les explications simples et concrètes de Pablo & Clodio.
 
L'immersion dans cet endroit magnifique et reculé des Cévennes a notamment favorisé les échanges et provoqué des réflexions intéressantes entre participants, le tout dans une très bonne ambiance de travail. Ces quelques jours m'ont permis de développer une vision nouvelle quant au déroulement d'un projet (pro ou perso d'ailleurs) avec les différents process à mettre en oeuvre pour aboutir à des vraies solutions (prise de décision, auto-organisation, user stories...)"

-- **Maiwenn Leclercq**

" Quand notre manager nous a annoncé la possibilité de participer au premier Raid Agile, nous avons tout de suite été motivé : il voulait alors convertir toutes les équipes en équipes SCRUM et nous voulions mieux connaitre les méthodes agiles ainsi qu’éventuellement trouver des réponses à nos questions et problématiques.

La page *le Raid Agile* se présentait comme une expérience différente d’une formation *classique*. Elle nous promettait des paysages à couper le souffle, de bons repas et beaucoup d’Agilité.

La présentation de la formation nous laissait perplexe et nous ne savions pas trop comment l’on devait s’y préparer. Nous savons maintenant qu’il suffit : d’avoir envie d’approfondir ses connaissances sur l’Agilité, d’avoir envie de partager le vécu et les  expériences d’autres personnes motivées, de ne pas oublier ses chaussures de rando et de ne pas manger durant les 3 jours qui précèdent la formation !

Après un départ un peu accidenté entre la grève de la SNCF et la grève du métro à Toulouse, nous sommes finalement arrivés à la gare de Nîmes où Pablo et Claudio nous attendaient comme promis. La tempête de la veille nous avait préparé une arrivée au gîte assez spartiate: pas d’électricité, pas d’eau, pas de téléphone, pas de Wifi, AU SECOURS !!!!! J Cependant un grand gite agréable avec des chambres confortables et une grande cheminée qui nous permettrait de nous chauffer en attendant que l’électricité revienne (pour rassurer le lecteur, l’électricité est revenue l’après-midi du premier jour et le Wifi au deuxième jour : ce que la plus part d’entre nous a presque regretté car nous avions apprécié le fait *d’être coupé du monde*).

Nous avons tout de suite remarqué la bonne complémentarité de nos formateurs : Pablo amène le coté dynamique et Claudio un coté plus posé, ce qui fait un bon équilibre pour qu’une formation de 3 jours ait tous les ingrédients pour être réussie.

Sans passer en revue tous les ateliers, nous en avons fait une trentaine. Des ateliers divers et variés pour que tout le monde y trouve son compte et surtout des ateliers dynamiques où la participation de tous est nécessaire pour que la dynamique d’équipe s’établisse tout de suite dans le groupe. Un groupe de personnes avec des histoires, des objectifs et des professions différentes, unies par l’envie de vivre l’Agilité en équipe.

Les repas aussi étaient des moments vécus en équipe : de la préparation de la table jusqu’au dessert … Ceci a aidé à créer une ambiancé confortable pour tous, depuis le premier jour.  Les menus étaient copieux : pâté, saucisson ainsi qu’une grande variété de produits gourmands et desserts typiques de la région.

Lors des pauses, la dynamique d’équipe était toujours vivante : de la partie de pétanque, à la partie de ping-pong, jusqu’à l’allumage de la cheminée (ceci dit, il était aussi bien sûr possible de s’isoler si l’on en avait besoin).

La randonnée était une des activités les plus attendues : d’ailleurs ce mélange d’activé sportive avec un atelier Agile sur le Mont Brion, a fortement plu à tous les participants.

Avant le raid, nous étions 5 personnes de la même entreprise prêtes à apprendre de nombreuses de choses sur l’agilité mais en même temps inquiètes par le format de la formation.

Après la formation, nous avons pris le train avec l’envie : de partager notre vécu avec le reste de l’équipe, de mettre en application certaines pratiques apprises ainsi que de nous mettre au régime et manger des fruits et des légumes après les repas copieux des trois derniers jours J

Dans l’ensemble, le Raid Agile est une formation riche en ateliers, riche en Agilité, riche en nouveautés, riche en calories J, mais surtout, et le plus important, riche en échanges humains : une expérience à vivre ! "

-- **Claire, Goretti, Cyril, Vincent et Olivier**




