---
title: Info
--- 


### Prochain meetup : 10 octobre 2018, 
## "La partie immergée de l'UX" par Elodie Delaisement 

![Iceberg UX](/images/20181010/iceberg.jpg)

Retour d'expérience sur quelques années de pratique UX en équipe Agile. 

UX, c'est le job à la mode, nous dit Elodie, tous les POs en veulent dans leur équipe, mais sont-ils vraiment prêts à travailler avec eux ? Est-ce que l'arrivée d'un UX dans l'équipe crée magiquement de la valeur pour l'utilisateur ? 

Ce soir là, Elodie vous proposera une vision élargie de l'UX qui ne se cantonnera pas à la production de *personae*, *journeys* et *design*. Un retour à sa mission essentielle : aider le PO à créer de la valeur pour l'utilisateur tout en limitant les risques pour l'entreprise.

![Elodie Delaisement](/images/20181010/elodie.jpg)

Le meetup sera animé par [Elodie Delaisement](https://twitter.com/hellomellowfr) !   
Les [inscriptions aux meetups c'est ici](https://www.meetup.com/School_of_PO/).

Les meetups suivants seront le 14 novembre et le 12 décembre. Qu'on se le dise !


### Ce soir meetup : 12 septembre 2018, 
## "L'expérience utilisateur démystifiée" par Aurélie Roland

Dès les années 80, Donald Norman écrivait sur les prémisses de la Conception Centrée Utilisateur. Aujourd’hui, la plus value pour un service de se concevoir autour de l’Expérience Utilisateur n’est plus à débattre. Ainsi de “nouveaux métiers” voient le jour, se spécifient et deviennent une des clefs pour un produit réussi. C’est le cas notamment du design. Cependant je constate assez souvent que ces termes “d’expérience utilisateur”, de “designer” peuvent être impressionnant et mal-compris par les différentes parties d’un même service .
Ce soir, j’aimerais que nous démystifions la notion d’Expérience Utilisateur et que vous quittiez ce talk avec la conviction que vous aussi êtes garants de l’Expérience qu’un utilisateur aura de votre service.

Le meetup sera animé par [Aurélie Rolland](https://www.linkedin.com/in/aur%C3%A9lie-rolland-elia/) !   
Les [inscriptions aux meetups c'est ici](https://www.meetup.com/School_of_PO/).

![Aurélie Rolland](/images/speakers/aurelie-rolland.jpg)

## Conférence 2019 !

Ca y est nous lançons les inscriptions à la conférence **school of po 2019** c'est ici : http://2019.schoolofpo.com

![Product Révolution](/images/conference/revolution.png)

## PO Retreat à la rentrée ?  

Nous avons dix candidats au *PO Retreat* (http://schoolofpo.com/poretreat/), encore deux et nous lançons la réflexion sur quand et où. 

