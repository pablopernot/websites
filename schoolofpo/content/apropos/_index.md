---
title: A propos
layout: content
headline: "School of PO mais encore ?"
---

## La School of PO ?

Les entreprises pensent qu'elles manquent d'argent et de temps. Quand on sait que deux tiers des choses fabriquées ne sont jamais ou rarement utilisées, nous on se dit que c'est surtout du **bon produit** que l'on manque. 
										
>Comme le disait Kung-Fu (David Carradine) suite à l'enseignement de Maître PO : "I do not seek answers, but rather to understand the question", que l'on traduira ici par : *je ne cherche pas des solutions mais à comprendre le problème*.


Parcours du **PO (*Product Owner*), *Lean Startup*, *Design Thinking*, Stratégie et tactique (*Impact Mapping*, *User Story Mapping*), *Storytelling***, etc. Nous espérons faire de ce lieu d'échange, de conversations, de rencontres, un espace convivial et riche d'enseignements. Nous sommes tous des petits scarabées. 

<span class="image center">
![Joker](/images/joker.png)
</span>

School of PO by <a http://benextcompany.com>beNext</a>
