---
title: Formations
layout: content
headline: "Be your potential"
---


## FORMATION SCHOOL OF PO 4-5 février 2019

### Programme

![Formation PO](/images/formations/formation-po.jpg)


* Date : 4-5 février 2019
* Lieu : 12 rue Vivienne, 75002 Paris 
* Tout public.   
* Nous sommes un organisme agréé. 

**Tarif : 1500€ HT**. 

### S'inscrire 

Remplir le formulaire en bas de page pour nous contacter. 

### Formateur : LESIEUR Nils

**C'est quoi le coaching pour toi ?** Aider l’équipe à devenir autonome dans l’aménagement de ses tâches et de leur amélioration. Comprendre, écouter, questionner, autant d’actions qui donneront du sens à vos activités et vous aideront à vous organiser. Responsabiliser et rendre autonome votre équipe en toute bienveillance, voilà mon leitmotiv.

**Pourquoi tu es coach ?** Après neuf ans dans le développement .Net (développeur & scrum master) et trois ans comme responsable des opérations dans un cabinet de conseil, je deviens coach RH/agile au sein de beNext. Fan de l’approche Solution Focus et d’amélioration continue, je souhaite vous amener à prendre en considération votre potentiel et celui de votre organisation en vous apportant mes regards de développeur, scrum master et manager.

![Nils](/images/formations/nils.jpg)

« Le meilleur moyen d'atteindre son objectif au rugby est d'aider les autres à atteindre le leur » - Johny Wilkinson