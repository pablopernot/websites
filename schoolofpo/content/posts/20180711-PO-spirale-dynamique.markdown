---
date: 2018-07-11
type: meetup
slug: po-dans-la-spirale-dynamique
background: "20180711/meetup7.jpg"
title: "Le PO dans la spirale dynamique"
synopsis: "La spirale dynamique est une grille de lecture de la complexité (et pas de la qualité) de nos organisations et des personnes qui les composent. Elle décrit des types de postures, de croyances, de comportements qu'elle classifie par niveaux de maturité en quelque sorte. Deux points importants : on doit transiter par tous les niveaux pour avancer, on ne peut pas sauter d'étape ; l'étape suivante se construit dans le rejet de l'étape précédente. La Spirale dynamique nous apprend à lire des comportements et à observer leur évolution dans les organisations. Que doit comprendre le PO dans tout cela ? Dans quel type d'organisation vit-elle ? Comment devrait-il agir ? Comment articuler son dialogue avec son organisation et les gens qui la composent ?"
---


## Liens

- http://spiraldynamicsintegral.nl/en/valuesystems/
- http://www.thenextevolution.com/spiral-dynamics/
- http://www.cruxcatalyst.com/2013/09/26/spiral-dynamics-a-way-of-understanding-human-nature/
- https://www.horsnormeetaccomplis.fr/spiral-dynamics-decoder-le-monde-et-changer-de-vie/
- http://www.slideshare.net/xomox/spiral-dynamics-learning-9143356
- https://fr.slideshare.net/magx68/spiral-dynamics-introduction-1110600
- https://fr.slideshare.net/SocEntMelbourne/25-june-2018-enkel-u-spiral-dynamics-masterclass
- http://spiraldynamicsintegral.nl/wp-content/uploads/2013/09/McDonald-Ian-Introduction-to-Spiral-Dynamics-1007.pdf

## Par

* <a href=https://twitter.com/ygrenzinger target=_blank>Yannick Grenzinger</a>
* <a href=https://twitter.com/pablopernot target=_blank>Pablo Pernot</a>

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Quelques photos

<div class="row 50 uniform">
		
<div class="4u"><span class="image fit"><img src="/images/20180711/meetup1.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20180711/meetup2.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20180711/meetup3.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20180711/meetup4.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20180711/meetup5.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20180711/meetup7.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20180711/meetup6.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20180711/meetup8.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20180711/meetup9.jpg" alt="school of po"></span></div>


</div>



