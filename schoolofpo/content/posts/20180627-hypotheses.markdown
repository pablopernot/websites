---
date: 2018-06-27
type: meetup
slug: tout-est-hypothese
background: "20180627/meetup7.jpg"
title: "Tout est hypothèse"
synopsis: "90% des PO que je vois me disent qu'ils ont du mal à expérimenter. Une présentation sur les hypothèses peut aider à expérimenter et terminer les débats sur 'doit-on faire cette fonctionnalité ou non'."
---


## Liens
* [Les Slides de Christopher](https://www.slideshare.net/christopherparola/tout-est-hypothese-product-management-school-of-po-104069970)


<iframe src="//www.slideshare.net/slideshow/embed_code/key/im4spdTwj9IY3" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/christopherparola/tout-est-hypothese-product-management-school-of-po-104069970" title="Tout est hypothese - Product Management - School Of PO" target="_blank">Tout est hypothese - Product Management - School Of PO</a> </strong> from <strong><a href="https://www.slideshare.net/christopherparola" target="_blank">Christopher Parola</a></strong> </div>

## Par

* <a href=https://twitter.com/chrisparola target=_blank>Christopher Parola</a>

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Quelques photos

<div class="row 50 uniform">
		
<div class="4u"><span class="image fit"><img src="/images/20180627/meetup1.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20180627/meetup2.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20180627/meetup3.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20180627/meetup4.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20180627/meetup5.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20180627/meetup7.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20180627/meetup6.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20180627/meetup8.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20180627/meetup9.jpg" alt="school of po"></span></div>


</div>



