---
date: 2017-01-18
type: meetup
slug: le-product-owner-contraire
background: "20170118/meetup1.png"
title: 'Le product owner contraire '
synopsis: "Comment en se projetant sur un mauvais PO on pourrait découvrir le bon ? A l'image du site VDM (http://www.viedemerde.fr/), reprenons nos anecdotes de la vie quotidienne pour illustrer le parfait #PODM... Ou tout ce qu'il ne faut pas faire pour réussir dans le rôle de Product Owner !"
---

## Les liens 

* Les <a href=http://docs.google.com/presentation/d/1v17ZlS9ePsJ71PJNEpVYpYasCs6pO8G3sEs-S9Oa82I/mobilepresent#slide=id.g19ca3a8112_0_177>slides de l'atelier</a>
* <a href=http://lucyinthescrum.com/po-de-mrde/>PO de Marde, un article de Lucie</a>

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Les photos des présentations

<div class="row 50 uniform">

<div class="4u"><span class="image fit"><img src="/images/20170118/meetup2.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170118/meetup3.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170118/meetup13.jpg" alt=""></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170118/meetup5.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170118/meetup7.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170118/meetup8.jpg" alt=""></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20170118/meetup17.jpg" alt=""></span></div>


<div class="4u"><span class="image fit"><img src="/images/20170118/meetup10.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170118/meetup11.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170118/meetup12.jpg" alt=""></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170118/meetup16.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170118/meetup19.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170118/meetup18.jpg" alt=""></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20170118/meetup2.jpg" alt=""></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170118/meetup13.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170118/meetup14.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170118/meetup15.jpg" alt=""></span></div>

</div>

## Les photos des "commandements" qui ont émergés

<div class="row 50 uniform">

<div class="4u"><span class="image fit"><a href=/images/20170118/meetupcom1.jpg>
<img src="/images/20170118/meetupcom1.jpg" alt=""></a></span></div>
<div class="4u"><span class="image fit"><a href=/images/20170118/meetupcom2.jpg>
<img src="/images/20170118/meetupcom2.jpg" alt=""></a></span></div>
<div class="4u$"><span class="image fit"><a href=/images/20170118/meetupcom3.jpg>
<img src="/images/20170118/meetupcom3.jpg" alt=""></a></span></div>
<div class="4u"><span class="image fit"><a href=/images/20170118/meetupcom4.jpg>
<img src="/images/20170118/meetupcom4.jpg" alt=""></a></span></div>
<div class="4u"><span class="image fit"><a href=/images/20170118/meetupcom5.jpg>
<img src="/images/20170118/meetupcom5.jpg" alt=""></a></span></div>

</div>


