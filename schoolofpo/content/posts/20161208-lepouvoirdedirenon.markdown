---
date: 2016-12-08
type: meetup
slug: le-pouvoir-de-dire-non
background: "20161208/meetup1.jpg"
title: 'Le pouvoir de dire non'
synopsis: "Pourquoi savoir dire non est une vraie qualité de PO ? Comment y arriver ?"
---

## Le document pdf qui nous a servit de support avec les textes et les dialogues : 

* <a href=/images/20161208/le-pouvoir-de-dire-non.pdf>Le pouvoir de dire non</a>

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Les photos des présentations

<div class="row 50 uniform">

<div class="4u"><span class="image fit"><img src="/images/20161208/meetup6.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161208/meetup3.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161208/meetup11.jpg" alt=""></span></div>

<div class="4u"><span class="image fit"><img src="/images/20161208/meetup4.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161208/meetup7.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161208/meetup10.jpg" alt=""></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20161208/meetup8.jpg" alt=""></span></div>


<div class="4u"><span class="image fit"><img src="/images/20161208/meetup12.jpg" alt=""></span></div>
<div class="4u"><span class="image fit"><img src="/images/20161208/meetup23.jpg" alt=""></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20161208/meetup9.jpg" alt=""></span></div>

</div>


## Les photos des "openspace"

les sujets de l'open space sont les non de la présentation : <a href=/images/20161208/le-pouvoir-de-dire-non.pdf>Le pouvoir de dire non</a>

<div class="row 50 uniform">
<div class="12u$"><span class="image fit"><img src="/images/20161208/meetup2.jpg" alt=""></span></div>


<div class="4u"><span class="image fit"><a href=/images/20161208/meetup15.jpg>
<img src="/images/20161208/meetup15.jpg" alt=""></a></span></div>
<div class="4u"><span class="image fit"><a href=/images/20161208/meetup16.jpg>
<img src="/images/20161208/meetup16.jpg" alt=""></a></span></div>
<div class="4u$"><span class="image fit"><a href=/images/20161208/meetup17.jpg>
<img src="/images/20161208/meetup17.jpg" alt=""></a></span></div>


<div class="4u"><span class="image fit"><a href=/images/20161208/meetup18.jpg>
<img src="/images/20161208/meetup18.jpg" alt=""></a></span></div>
<div class="4u"><span class="image fit"><a href=/images/20161208/meetup21.jpg>
<img src="/images/20161208/meetup21.jpg" alt=""></a></span></div>
<div class="4u$"><span class="image fit"><a href=/images/20161208/meetup20.jpg>
<img src="/images/20161208/meetup20.jpg" alt=""></a></span></div>



<div class="4u"><span class="image fit"><a href=/images/20161208/meetup19.jpg>
<img src="/images/20161208/meetup19.jpg" alt=""></a></span></div>
<div class="4u"><span class="image fit"><a href=/images/20161208/meetup22.jpg>
<img src="/images/20161208/meetup22.jpg" alt=""></a></span></div>
<div class="4u$"><span class="image fit"><a href=/images/20161208/meetup24.jpg>
<img src="/images/20161208/meetup24.jpg" alt=""></a></span></div>



<div class="4u"><span class="image fit"><a href=/images/20161208/meetup5.jpg>
<img src="/images/20161208/meetup5.jpg" alt=""></a></span></div>
<div class="4u"><span class="image fit"><a href=/images/20161208/meetup25.jpg>
<img src="/images/20161208/meetup25.jpg" alt=""></a></span></div>
<div class="4u$"><span class="image fit"><a href=/images/20161208/meetup26.jpg>
<img src="/images/20161208/meetup26.jpg" alt=""></a></span></div>


</div>


