Certifications
###############
:slug: certifications
:date: 2014-02-02
:template: certifications
:subtitle: Cela implique d'avoir tenu plus de 40 heures en compagnie de Pablo et Clodio, à faire des ateliers, à avoir des conversations et à apprendre de nouvelles pratiques issues des méthodes agiles et au-delà.

.. raw:: html
 
	<a href="#" class="image feature"><img src="/theme/images/certification.jpg" alt="Certification" /></a>


RAID AGILE #1 - Novembre 2014
------------------------------

* Anthony Cassaigne
* Claire Torres
* Cyril Merique
* Goretti Puig
* Jean-Claude Béard
* Maiwenn Leclercq
* Nicolas Jouve 
* Nicolas Montens
* Olivier Peyrusse
* Vincent Renaudie

RAID AGILE #2 - Mars 2015
------------------------------

* Cédric Ménétrier
* Claire Cassan 
* Dani Al Saab
* Didier Plaindoux
* Ghislaine Barbier
* Isabelle Conreaux 
* Jean-François Marronnier
* Joël Conraud 
* Luc Buatois
* Matthieu Dazy 
* Olivier Mariez
* Patxy Gascué
* Pierre-Jean Montroziés
* Serge Prétet 
* Sylvain Thenault 
* Yassine Zakaria 

RAID AGILE #3 - Juin/Juillet 2015
---------------------------------

* Alice Barralon
* Alice Chauvin
* Benjamin Dubois
* Cyrille François
* Elsa Villarubias
* Emmanuel Gringarten
* Eric Michel
* Florent Cayré
* Kamal Hami-Eddine
* Marie-Sophie Michel
* Romain Crunelle
* Sofiene Laadhar
* Sylvie Fabry
