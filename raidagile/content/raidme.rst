Raidme
######
:slug: raidme
:date: 2014-02-02
:template: simple
:maintitle: RAIDME
:subtitle: Le guide du raideur
:awesomefonticon: tree


.. raw:: html

	<section class="wrapper style4 container"><div class="content"><section>

Le "raidme" : le document sur les infos pratiques et le contenu du raid.

.. image:: /theme/images/3c.png
   :width: 400
   :alt: le guide du raideur
   :class: image center
   :target: /pdfs/raidme.pdf


`Télécharger le guide du raideur en cliquant ici`_.


.. _`Télécharger le guide du raideur en cliquant ici`: /pdfs/raidme.pdf

.. raw:: html

	</section></div></section>

