---
date: 2017-10-18
type: meetup
slug: scarabee-storytelling
background: "20171018/storytelling1.jpg"
title: 'Scarabée Storytelling'
synopsis: "Petit scarabée, dans l’art du PO et son chemin vers la sagesse, abandonner le langage Gherkin tu dois. Gherkin, ou GIVEN-WHEN-THEN-BUT, est la fourmi, alors que tu dois voir la fourmilière dans son ensemble, et bien au-delà. "
---

## L'annonce 

Petit scarabée, dans l’art du PO et son chemin vers la sagesse, abandonner le langage Gherkin tu dois. Gherkin, ou GIVEN-WHEN-THEN-BUT, est la fourmi, alors que tu dois voir la fourmilière dans son ensemble, et bien au-delà. Tu utilises “User Story”, mais tu n’utilise pas vraiment la vraie puissance et la portée du “Story Telling”. Comment une fourmi peut-elle communiquer avec les abeilles du Marketing, les papillons des Sales et les lions du Management ? Et comment faire passer ton message outre la forêt, dans le vrai monde du Marché ? Ne pas utiliser un langage spécifique tu dois. Utiliser un langage universel cela t’aidera. 

Lors de cet entraînement :

* Shu, les fondements visuellement et auditivement tu écouteras et feras tiens
* Ha, le maniement kinestésiquement tu pratiqueras
* Ri, la transcendance retrospectivement tu atteindras

## L'invité (par)

<a href=https://twitter.com/nyconyco target=_blank>Nicolas Vérité</a>

## Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

## Quelques photos


<div class="row 50 uniform">
	
	<div class="4u"><span class="image fit"><img src="/images/20171018/storytelling2.jpg" alt="school of po"></span></div>
	<div class="4u"><span class="image fit"><img src="/images/20171018/storytelling3.jpg" alt="school of po"></span></div>
	<div class="4u$"><span class="image fit"><img src="/images/20171018/storytelling4.jpg" alt="school of po"></span></div>
	
</div>




