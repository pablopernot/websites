Raid Agile Tribal
#################
:slug: raid-agile-tribal
:date: 2014-02-02
:template: tribal
:maintitle: RAID AGILE TRIBAL
:subtitle: Le Raid Agile tribal, c'est comme le Raid Agile,<br/> mais pour une seule tribu.

Le raid agile tribal
====================

Le Raid Agile tribal, c'est comme le Raid Agile, mais pour une seule tribu.

Il reprend tout ce qui a fait le succès des Raids Agiles en Cévennes :

.. raw:: html

	<ul>
   	<li><input type=checkbox checked>Un lieu sympa pour que tout le monde se sente bien</li>
  	<li><input type=checkbox checked>Une durée qui inclut au moins 2 soirées ensemble pour avoir vraiment le temps de partager</li>
  	<li><input type=checkbox checked>Deux animateurs quinquados, toujours rock'n roll, Pablo et Claudio</li>
  	<li><input type=checkbox checked>Un programme de formation qui s'appuie sur le cadre Scrum et montre comment l'adapter et le compléter (story mapping, plan de release, etc.)</li>
  	</ul>

Comme le Raid Agile tribal accueille des participants venant tous de la même organisation, celle-ci peut choisir le lieu et la date, et demander à Pablo et Claudio d'adapter le programme à son contexte. 

Cette formule permet d'utiliser un vrai projet de l'organisation pour les ateliers, plutôt que l'exemple animalier Peetic. 

Le premier Raid Agile tribal s’est déroulé en juin 2016 sur la Côte d’Opale. La tribu en question était Xee_, une startup lilloise dont le projet fou est de rendre les véhicules intelligents et de faciliter la vie des automobilistes.

Quelques mots sur l'histoire de Xee_ :

"Nous avons tous déjà cherché notre voiture sur un parking, laissé les phares allumés toute une nuit, oublié de faire le plein, ou même avoir peur de prêter notre voiture à nos enfants… et bien c’est en ayant lui‐même vécu ces scènes qu’un matin de janvier, Stéphane, fondateur d’Eliocity a décidé de se lancer dans ce projet passionnant : faire parler les voitures pour se libérer l’esprit de toutes ses contraintes.

Depuis janvier 2012, Eliocity développe des solutions innovantes pour le conducteur. Objectif : que toutes les voitures vivent dans leur temps !"

http://www.xee.com/

La première journée, tous les membres de la tribu Xee_ sont venus au Raid (35 personnes). Le Raid s'est poursuivi avec les 21 personnes du développement et du marketing. Les photos plus bas !

.. _Xee: http://www.xee.com

