---
date: 2017-04-26
slug: intervision
type: meetup
background: "20170426/meetup1.jpg"
title: 'Intervision de PO'
synopsis: "L'intervision est un dispositif particulier de rencontres entre pairs, des professionnels et praticiens des secteurs sanitaires, sociaux, médico-sociaux, éducatifs et judiciaires... Nous dit Wikipedia (https://fr.wikipedia.org/wiki/Intervision). Nous vous la proposons pour les Product Owner sous la forme d'une 'PO clinique'."
---

## Les liens 

Tout est résumé là dans l'article de Lucie : 

* [#Meetup – Intervision / Solution Focus](http://lucyinthescrum.com/intervision-du-product-owner/)
* [Perfection Game, les Core Protocols](http://www.mccarthyshow.com/online/)

##Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

##Quelques photos

<div class="row 50 uniform">

<div class="4u"><span class="image fit"><img src="/images/20170426/meetup1.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170426/meetup8.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170426/meetup3.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170426/meetup4.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170426/meetup9.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170426/meetup6.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170426/meetup7.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170426/meetup2.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170426/meetup5.jpg" alt="school of po"></span></div>

</div>
