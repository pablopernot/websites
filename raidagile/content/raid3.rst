Raid3
######
:slug: raid3
:date: 2014-02-02
:template: raid3
:maintitle: RAID AGILE #3
:subtitle: Quelques souvenirs du **raid agile #3**

.. raw:: html

	<section class="wrapper style2 container special">
	<div class="12u">
	<header>
	<h2>Articles</h2> 
	</header>
				

`Souvenirs du Raid Agile #3 par Claude Aubry`_	

.. raw:: html
	
	</div></section>

.. _`Souvenirs du Raid Agile #3 par Claude Aubry`: http://www.aubryconseil.com/post/Souvenirs-du-Raid-Agile-3


.. raw:: html 

	<section class="wrapper style1 container special-alt">
	<div class="row half">	
	<div class="12u">

Témoignages
===========

"Étant le plus jeune du groupe et participant à ma première formation, j'ai eu le privilège de tomber sur le RAID Agile, de loin la meilleure formation qui puisse exister ! 3 jours de formation dans un cadre extra-ordinaire dans les hauteurs des Cévennes, là ou même le réseau n'a osé s'aventurer, Pablo et Claude nous ont pleinement immergé dans l'agilité avec des visions parfois opposées quand bien même convergentes. "

-- **Sofiène Laadhari**


"L'agilité n'est pas une méthodologie, elle dépend de son contexte, alors rien de tel qu'une immersion complète de 3 jours dans l'agilité pour en saisir toutes les nuances issues des deux différentes palettes de Pablo et Clodio. Mais surtout pour échanger entre nous et se rendre compte que nos problèmes ne sont pas si différents et pour découvrir comment chacun à ou va adapter l'agilité à son contexte pour les résoudre."

-- **Cyrille François**

"La **meilleure** formation que j'ai eue. Une immersion de 3 jours dans un cadre magnifique, des moments de partage et de réflexion qui amènent à une remise en question du 'management' et de la 'gestion de projet' tels que je les avais connus jusque là. Ce n'est pas seulement une formation professionnelle mais c'est aussi une remise en question personnelle qui recentre l'humain au centre des priorités. Merci également à Alex Boutin et Stéphane Langlois pour les discussions partagées."

-- **Alice Barralon** 


Sketches
========

Les sketches de Cyrille_. Cliquez dessus pour agrandir.  

Journée 1
---------

.. image:: /theme/images/raid3/sketch-cyrille-j1.jpg
   :width: 800
   :alt: Journée 1
   :class: image center
   :target: /theme/images/raid3/sketch-cyrille-j1.jpg

.. image:: /theme/images/raid3/sketch-cyrille-j1-2.jpg
   :width: 800
   :alt: Journée 1
   :class: image center
   :target: /theme/images/raid3/sketch-cyrille-j1-2.jpg



Journée 2
---------

.. image:: /theme/images/raid3/sketch-cyrille-j2.jpg
   :width: 800
   :alt: Journée 2
   :class: image center
   :target: /theme/images/raid3/sketch-cyrille-j2.jpg

Journée 3
---------

.. image:: /theme/images/raid3/sketch-cyrille-j3.jpg
   :width: 800
   :alt: Journée 3
   :class: image center
   :target: /theme/images/raid3/sketch-cyrille-j3.jpg




.. raw:: html 

	</div></div></section>



.. _Cyrille: https://twitter.com/CfrancCyrille
