---
title: Equipe
layout: content
headline: "le gang"
---


**Pablo Pernot**

<span class="image left">
![Pablo Pernot](/images/pablo.jpg)
</span>


Coach holistique accompagnant des organisations agiles ou souhaitant le devenir. Mais aussi coach de coach de coach de coach. Ce qui n’est pas un job facile. Il faut savoir prendre du recul, aimer le comique de répétition. Et tout bien réfléchit peut-être plus mentor que coach.

Après une maîtrise sur les Monty Python, un DEA sur le non-sens et l’absurde, l’entame d’un doctorat sur les comiques cinématographiques il parait normal que Pablo se soit lancé dans le management et la conduite du changement, c’est -finalement- une suite logique. Actuellement un acteur expérimenté en management organisationnel (holacratie, sociocratie, agile, lean, kanban), conduite du changement, et agilité chez beNext, il accompagne des grands groupes dans leurs façons d'aborder leurs trajectoires agiles.

https://areyouagile.com

---

**Dragos Dreptate**

<span class="image left">
![Dragos Dreptate](/images/dragos.jpg)
</span>


À l'instar du petit garçon dans "les habits neufs de l'empereur", Dragos aide les organisations à se poser les bonnes questions et à trouver des réponses dans leur chemin de transformation qui passe par une prise de conscience de l'écart entre ce que les organisations pensent être, ce qu'elles sont vraiment et ce qu'elles veulent devenir.

Sa mission: bâtisseur de possible, bâtisseur de demain. Sa raison d'être: donner du sens et aider les gens à trouver la joie et la fierté dans ce qu'ils accomplissent.

http://andwhatif.fr