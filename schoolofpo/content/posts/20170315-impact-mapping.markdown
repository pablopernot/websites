---
date: 2017-03-15
type: meetup
slug: impact-mapping
background: "20170315/meetup1.jpg"
title: 'Impact Mapping'
synopsis: "L'impact mapping est une technique utilisée pour réaliser la cartographie de votre stratégie sous la forme d'une carte mentale (mindmap). L’objectif de cette cartographie de stratégie est de comprendre où on veut aller et par où passer pour s’assurer au plus vite que le chemin est le bon, mettre en évidence les chemins critiques, les chemins clefs pour valider nos hypothèses."
---

Avec notre première invitée : [Géraldine Legris](http://www.theobserverself.com/) qui nous vient du côté de chez [Soat](http://www.soat.fr/). 

## Les liens 

* [Impact Mapping par Géraldine Legris](http://www.theobserverself.com/pratiques/impact-map/)
* [Cartographie de stratégie, l'impact mapping par Pablo Pernot](https://www.areyouagile.com/2017/02/cartographie-strategie-impact-mapping/)
* [Cartographie de plan d’action(user story map) revisitée par Pablo Pernot](https://www.areyouagile.com/2017/02/cartographie-plan-action-revisitee/)   
* [Impact Mapping Workshop par Gojko Adzic](https://gojko.net/news/2016/05/09/open-impact-mapping.html)

## Les liens de la petite saynète entre Marie-Caroline & Jacques (Alias Géraldine & Dragos)

* ["Impact Mapping dans le XVIème", l'affiche](/images/20170315/affiche.jpg)
* ["Impact Mapping dans le XVIème", le texte](/images/20170315/ImpactmappingSchoolofPO.pdf)
* [Carte "Aménager notre appartement"](/images/20170315/appartement.pdf)

##Le flux twitter

* [#schoolofpo OR @schoolofpo](https://twitter.com/search?q=%23schoolofpo%20OR%20%40schoolofpo&src=typd)

##Les photos de la petite pièce entre Marie-Caroline & Jacques (Alias Géraldine et Dragos)

<div class="row 50 uniform">

<div class="4u"><span class="image fit"><img src="/images/20170315/meetup9.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170315/meetup8.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170315/meetup4.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170315/meetup6.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170315/meetup7.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170315/meetup15.jpg" alt="school of po"></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20170315/meetup2.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170315/meetup12.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170315/meetup13.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170315/meetup14.jpg" alt="school of po"></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20170315/meetup19.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170315/meetup16.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170315/meetup18.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170315/meetup17.jpg" alt="school of po"></span></div>

<div class="12u$"><span class="image fit"><img src="/images/20170315/meetup5.jpg" alt="school of po"></span></div>

<div class="4u"><span class="image fit"><img src="/images/20170315/meetup3.jpg" alt="school of po"></span></div>
<div class="4u"><span class="image fit"><img src="/images/20170315/meetup10.jpg" alt="school of po"></span></div>
<div class="4u$"><span class="image fit"><img src="/images/20170315/meetup11.jpg" alt="school of po"></span></div>

</div>



