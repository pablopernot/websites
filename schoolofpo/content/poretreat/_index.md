---
title: PO Retreat
layout: content
headline: "Amélioration par les pairs"
---


![PO Retreat](/images/poretreat/poretreat.jpg)

## PO RETREAT 

Def. Groupe de PO qui conversent, aiguisent leurs outils entre pairs, échangent un savoir faire et un savoir être durant deux jours, isolés du monde. 

## Principes pour monter cet événement 

D'abord QUI avant QUAND et OU. Nous construisons donc une liste de **12 personnes** (*a priori*). Quand elle est constituée nous décidons ensemble de *quand* et *où* (autour de Paris a priori, mais on ne sait jamais). 

On est 15. Nous lançons l'organisation (groupe Whatsapp). Les inscriptions restent ouvertes.  

* Alice Barralon
* Aurélie Gauthier
* David Claveyrolles
* Dragos Dreptate
* Elodie Delaisement
* Florian Ferbach
* Isabelle Blasquez
* Layouni Riad
* Montaine Marteau
* Nicolas Beladen
* Nicolas Potier
* Nils Lesieur
* Pablo Pernot 
* Rachel Jolin Dubois
* Vincent Loustau 





La liste est constituée, nous conversons sur Whatsapp pour définir : 

* Nous cherchons un lieu (hôtel + repas).
* Nous cherchons une date.
* Chacun paie son inscription ou nous centralisons le paiement. 
* On se retrouve. 

### Ouverture des inscriptions 

A vous de nous joindre avec le formulaire en bas de page et avec pour seule condition d'être vraiment présent lors de l'événement (chacun paie ses frais).

*merci à https://htck.github.io/bayeux/#!/ pour l'image tapisserie* 
